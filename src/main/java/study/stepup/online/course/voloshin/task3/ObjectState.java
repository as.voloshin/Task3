package study.stepup.online.course.voloshin.task3;

import java.lang.reflect.Field;
import java.util.HashMap;


public class ObjectState {
    private final HashMap<Field, Object> fields;

    public <T> ObjectState(T obj) throws IllegalAccessException {
        fields = new HashMap<>();
        for (Field f : obj.getClass().getDeclaredFields()) {
            f.setAccessible(true);
            fields.put(f, f.get(obj));
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ObjectState that = (ObjectState) o;
        if (this.fields.size() != that.fields.size()) {
            return false;
        }
        for (Field f : this.fields.keySet()) {
            if ((this.fields.get(f) == null && that.fields.get(f) != null) ||
                    (this.fields.get(f) != null && that.fields.get(f) == null)) {
                return false;
            }
            if (this.fields.get(f) != that.fields.get(f)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return fields.hashCode();
    }

}
