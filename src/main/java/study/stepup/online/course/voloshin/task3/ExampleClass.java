package study.stepup.online.course.voloshin.task3;


import java.util.concurrent.TimeUnit;

public class ExampleClass implements ExampleClassable {
    private int fieldInt;
    private String fieldString;

    public ExampleClass() {
        this.fieldString = "Test";
        this.fieldInt = 0;
    }

    @Cache(expiration = 5, timeUnit = TimeUnit.SECONDS)
    public int getFieldInt() {
        System.out.println("|   Direct call,");
        System.out.println("V   cache not used.");
        return fieldInt;
    }

    @Mutator
    public void setFieldInt(int fieldInt) {
        this.fieldInt = fieldInt;
    }

    @Cache(expiration = 1000)
    public String getFieldString() {
        System.out.println("|   Direct call,");
        System.out.println("V   cache not used.");
        return fieldString;
    }

    @Mutator
    public void setFieldString(String fieldString) {
        this.fieldString = fieldString;
    }

    @Override
    public String toString() {
        return "ExampleClass{" +
                "fieldString='" + fieldString + '\'' +
                '}';
    }

}
