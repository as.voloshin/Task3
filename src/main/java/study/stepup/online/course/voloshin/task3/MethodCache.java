package study.stepup.online.course.voloshin.task3;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Semaphore;

public class MethodCache {
    static final int CACHE_SIZE_LIMIT = 1024;
    final long expirationTimeInMs;
    private final Semaphore cacheCleaner = new Semaphore(1);
    private final ConcurrentHashMap<ArrayList<Object>, CacheEntry> cachedValues = new ConcurrentHashMap<>();
    int cacheMaximalSize;

    public MethodCache(long expirationTimeInMs) {
        this.cacheMaximalSize = 16;
        this.expirationTimeInMs = expirationTimeInMs;
    }

    public CacheEntry getCacheEntry(ArrayList<Object> key) {
        return cachedValues.get(key);
    }

    public void putCacheEntry(ArrayList<Object> key, Object value) {
        cachedValues.put(key, new CacheEntry(value));
    }

    public void updateCacheEntry(ArrayList<Object> key) {
        CacheEntry cacheEntry = cachedValues.get(key);
        if (cacheEntry != null) {
            cacheEntry.updateTimestamp();
        }
    }

    public void checkCacheSize() {
        if (cachedValues.size() > cacheMaximalSize * 0.9) {
            if (cacheCleaner.tryAcquire()) {
                CacheCleaner thread = new CacheCleaner(this);
                thread.start();
            }
        }
    }


    static class CacheCleaner extends Thread {
        final MethodCache methodCache;

        public CacheCleaner(MethodCache methodCache) {
            this.methodCache = methodCache;
        }

        @Override
        public void run() {
            methodCache.cachedValues.keySet().forEach(key -> {
                CacheEntry cacheEntry = methodCache.getCacheEntry(key);
                if (cacheEntry != null) {
                    if (cacheEntry.isExpired(methodCache.expirationTimeInMs)) {
                        methodCache.cachedValues.remove(key, cacheEntry);
                    }
                }
            });

            if (methodCache.cachedValues.size() > methodCache.cacheMaximalSize * 0.9) {
                methodCache.cacheMaximalSize = Math.min(2 * methodCache.cacheMaximalSize, CACHE_SIZE_LIMIT);
            }
            if (methodCache.cachedValues.size() > CACHE_SIZE_LIMIT) {
                throw new OutOfMemoryError();
            }
            methodCache.cacheCleaner.release();
        }
    }

}
