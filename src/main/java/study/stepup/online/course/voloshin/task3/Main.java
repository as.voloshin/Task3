package study.stepup.online.course.voloshin.task3;


public class Main {

    public static void main(String[] args) throws IllegalAccessException, InterruptedException {
        ExampleClassable ec = new ExampleClass();

        ec = CacheUtils.cache(ec);
        System.out.println(ec.getFieldString());
        System.out.println(ec.getFieldString());
        for (int i = 0; i < 8; i++) {
            ec.setFieldInt(i);
            System.out.println();
            System.out.println("i = " + i);
            System.out.println(ec.getFieldString());
            System.out.println(ec.getFieldString());
            Thread.sleep(80);
        }
        for (int j = 0; j < 16; j++) {
            ec.setFieldInt(j);
            System.out.println();
            System.out.println("j = " + j);
            System.out.println(ec.getFieldString());
            System.out.println(ec.getFieldString());
            Thread.sleep(100);
        }
        System.out.println("----");
        System.out.println(ec.getFieldString());
        Thread.sleep(1005);
        System.out.println(ec.getFieldString());
    }

}
