package study.stepup.online.course.voloshin.task3;

public interface ExampleClassable {
    int getFieldInt();

    void setFieldInt(int fieldInt);

    String getFieldString();

    void setFieldString(String fieldString);

    String toString();
}