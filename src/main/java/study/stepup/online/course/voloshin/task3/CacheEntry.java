package study.stepup.online.course.voloshin.task3;


public class CacheEntry {
    private final Object cachedValue;
    private long timestamp;

    public CacheEntry(Object cachedValue) {
        this.timestamp = System.currentTimeMillis();
        this.cachedValue = cachedValue;
    }

    public void updateTimestamp() {
        this.timestamp = System.currentTimeMillis();
    }

    public long getTimestamp() {
        return timestamp;
    }

    public Object getCachedValue() {
        return cachedValue;
    }

    public boolean isExpired(long expirationTimeInMs) {
        return timestamp + expirationTimeInMs < System.currentTimeMillis();
    }

    @Override
    public String toString() {
        return "CacheEntry{" +
                "timestamp=" + timestamp +
                ", cachedValue=" + cachedValue +
                '}';
    }
}
