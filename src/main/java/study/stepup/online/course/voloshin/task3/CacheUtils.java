package study.stepup.online.course.voloshin.task3;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

public class CacheUtils {

    @SuppressWarnings("unchecked")
    public static <T> T cache(T obj) throws IllegalAccessException {
        return (T) Proxy.newProxyInstance(
                obj.getClass().getClassLoader(),
                obj.getClass().getInterfaces(),
                new CacheHandler(obj));
    }

    static class CacheHandler implements InvocationHandler {

        final ConcurrentHashMap<Method, MethodCache> methodCaches = new ConcurrentHashMap<>();
        final Object obj;
        ObjectState currentObjectState;

        public CacheHandler(Object obj) throws IllegalAccessException {
            this.obj = obj;
            this.currentObjectState = new ObjectState(obj);
            for (Method m : obj.getClass().getMethods()) {
                if (m.isAnnotationPresent(Cache.class)) {
                    long expirationTimeInMs = TimeUnit.MILLISECONDS.convert(m.getAnnotation(Cache.class).expiration(),
                            m.getAnnotation(Cache.class).timeUnit());
                    this.methodCaches.put(m, new MethodCache(expirationTimeInMs));
                }
            }
        }


        public Method getClassMethod(Method interfaceMethod) throws NoSuchMethodException {
            return obj.getClass().getMethod(interfaceMethod.getName(), interfaceMethod.getParameterTypes());
        }


        public ArrayList<Object> getCacheKey(Object[] args) {
            ArrayList<Object> key = new ArrayList<>();
            key.add(currentObjectState);
            if (args != null) {
                Collections.addAll(key, args);
            }
            return key;
        }


        @Override
        public Object invoke(Object proxy, Method interfaceMethod, Object[] args)
                throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
            Method classMethod = getClassMethod(interfaceMethod);
            Object result;
            if (classMethod.isAnnotationPresent(Mutator.class)) {
                result = classMethod.invoke(obj, args);
                currentObjectState = new ObjectState(obj);
            } else if (classMethod.isAnnotationPresent(Cache.class)) {
                MethodCache methodCache = methodCaches.get(classMethod);
                ArrayList<Object> key = getCacheKey(args);
                CacheEntry cacheEntry = methodCache.getCacheEntry(key);
                if (cacheEntry != null && !cacheEntry.isExpired(methodCache.expirationTimeInMs)) {
                    result = cacheEntry.getCachedValue();
                    methodCache.putCacheEntry(key, result);
                } else {
                    result = classMethod.invoke(obj, args);
                    methodCache.putCacheEntry(key, result);
                    methodCache.checkCacheSize();
                }
            } else {
                result = classMethod.invoke(obj, args);
            }
            return result;
        }
    }
}
