package study.stepup.online.course.voloshin.task3;

import org.junit.jupiter.api.*;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

public class TestsForObjectStateClass {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @BeforeEach
    // Выполняем, чтобы методы класса TestClass не спамили в консоль.
    public void setUpStream() {
        System.setOut(new PrintStream(outContent));
        outContent.reset();
    }


    @AfterEach
    public void restoreStream() {
        System.setOut(originalOut);
    }


    @Test
    @DisplayName("Проверяем, что два состояния одного и того же объекта равны между собой.")
    public void checkTwoStatesOfUnchangedObjectAreEqual() throws IllegalAccessException {
        TestClassable obj = new TestClass();
        ObjectState state1 = new ObjectState(obj);
        ObjectState state2 = new ObjectState(obj);
        Assertions.assertEquals(state1, state2);
        Assertions.assertEquals(state2, state1);

        obj.setFieldInt1(2);
        ObjectState state3 = new ObjectState(obj);
        ObjectState state4 = new ObjectState(obj);
        Assertions.assertEquals(state3, state4);
        Assertions.assertEquals(state4, state3);

        obj.setFieldString("New state");
        ObjectState state5 = new ObjectState(obj);
        ObjectState state6 = new ObjectState(obj);
        Assertions.assertEquals(state5, state6);
        Assertions.assertEquals(state6, state5);
    }

    @Test
    @DisplayName("Проверяем, что состояния объекта до и после изменения не равны между собой.")
    public void checkTwoStatesOfChangedObjectAreEqual() throws IllegalAccessException {
        TestClassable obj = new TestClass();
        ObjectState state1 = new ObjectState(obj);
        obj.setFieldInt1(2);
        ObjectState state2 = new ObjectState(obj);
        Assertions.assertNotEquals(state1, state2);
        Assertions.assertNotEquals(state2, state1);

        obj.setFieldString("New state");
        ObjectState state3 = new ObjectState(obj);
        Assertions.assertNotEquals(state3, state1);
        Assertions.assertNotEquals(state3, state2);

        obj.setFieldInt2(-8);
        ObjectState state4 = new ObjectState(obj);
        Assertions.assertNotEquals(state4, state1);
        Assertions.assertNotEquals(state4, state2);
        Assertions.assertNotEquals(state4, state3);
    }

    @Test
    @DisplayName("Проверяем, что вызов метода не меняющего объект не меняет состояние объекта.")
    public void checkNonMutatingMethodDoesNotChangesObjectState() throws IllegalAccessException {
        TestClassable obj = new TestClass();
        ObjectState state1 = new ObjectState(obj);
        obj.getFieldInt1();
        ObjectState state2 = new ObjectState(obj);
        Assertions.assertEquals(state1, state2);

        obj.getFieldString();
        ObjectState state3 = new ObjectState(obj);
        Assertions.assertEquals(state3, state1);
        Assertions.assertEquals(state3, state2);

        obj.toString();
        ObjectState state4 = new ObjectState(obj);
        Assertions.assertEquals(state4, state1);
        Assertions.assertEquals(state4, state2);
        Assertions.assertEquals(state4, state3);
    }

    @Test
    @DisplayName("Проверяем, что присвоение прежних значений полям объекта даёт эквивалентное состояние объекта.")
    public void checkSettingFieldsToPreviousValuesDoesNotChangesObjectState() throws IllegalAccessException {
        TestClassable obj = new TestClass();
        ObjectState state1 = new ObjectState(obj);
        obj.setFieldInt1(5);
        obj.setFieldInt2(8);
        ObjectState state2 = new ObjectState(obj);
        obj.setFieldString("New value");
        ObjectState state3 = new ObjectState(obj);
        obj.setFieldString("Test");
        ObjectState state4 = new ObjectState(obj);
        Assertions.assertEquals(state4, state2);
        obj.setFieldInt2(1);
        obj.setFieldInt1(0);
        ObjectState state5 = new ObjectState(obj);
        Assertions.assertEquals(state5, state1);
    }

}
